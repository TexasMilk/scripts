#!/bin/bash

#generate a random value and convert it to hex
randHex=`printf '%x' $RANDOM`

#ensure the random hex is exactly 4 characters long by adding trailing zeros until it is
if [[ ${#randHex} -ne 4 ]]; then
	numChars=(4-${#randHex})
	while [[ $numChars -gt 0 ]]
	do
		randHex="${randHex}0"
		numChars=(4-${#randHex})
	done
fi

echo Random hex generated: $randHex

#get line for the entry NetworkAddress in .reg file
regLine=`cat networkadapter.reg | grep -Fi \"networkaddress ./networkadapter.reg`

#separate address value
currMAC=${regLine:18:12}

echo Current MAC: $currMAC

#replace last 4 characters with random values
newMAC=${currMAC:0:8}${randHex}

echo Changing MAC to $newMAC

#replace old address with newly generated one
sed -i "s/$currMAC/$newMAC/" ./networkadapter.reg && cat ./networkadapter.reg | grep -Fi \"networkaddress

echo
echo Importing networkadapter.reg to save new MAC

#invoke cmd to import the .reg file into the windows registry
/mnt/c/Windows/System32/cmd.exe /c REG IMPORT ./networkadapter.reg

read -n 1 -s -r -p "Success! Press any key to close this window...
"
