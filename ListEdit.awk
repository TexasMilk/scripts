BEGIN {$FS=":"; $OFS=" "}
{
	if ($1 ~ /[0-9]*:/)
	{
		$1 = "";
		sub(/^[\t]+/,"");
		print substr($0,2);
	}
}