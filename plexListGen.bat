@REM Written by Lily Wortham (@TexasMilk)
@echo off

title Plex Movie List File Generator

@REM Saving current directory for later
@set currentDir=%CD%

@REM Change to your plex install directory
echo Changing directory to Plex directory
cd "C:\Program Files (x86)\Plex\Plex Media Server"

@REM List all libraries as recognized by the Plex scanner
@echo on
"Plex Media Scanner.exe" -l
@echo.

@REM Set libraryID to the number of the library you want to dump
@echo off
@set /P libraryID= Please enter the ID number of the library: 
echo.

@REM Set fileName to the name of the file to be generated
@set /P fileName= Please enter the name of the library: 
echo.

echo Creating file "%fileName%.txt" in the batch file's defined directory

@REM Generate file at the desired directory (default it %UserProfile%\Desktop\PlexLists)
"Plex Media Scanner.exe" -l -c %libraryID% > %currentDir%\"%fileName%.txt"
echo.

echo File successfully generated as "%fileName%.txt"
echo.

@REM Go back to where we were
cd %currentDir%

@REM Run gawk script to remove the plex ids

gawk -f ListEdit.awk %fileName%.txt > %fileName%.tmp

@REM Run sort and put it back into %fileName%
sort %fileName%.tmp /o %fileName%.txt && del "%CD%\%fileName%.tmp"

echo All done!
pause
