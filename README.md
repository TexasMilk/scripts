# Files and Descriptions
+ plexListGen.bat
    + Generates a text file with a list of titles from a specified Plex library
+ ListEdit.awk
    + Removes plex's numerical identifiers from the created list
+ macchange.sh
    + Requires .reg file for target network adapter
    + Uses bash via WSL to edit a .reg file to change the NetworkAddress entry for an Intel I211 Gigabit Network Adapter in the Windows registry to randomize the last four characters
